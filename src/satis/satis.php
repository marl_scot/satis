<?php
/**
 * Compile a satis config file and run satis
 *
 * Provides a class to allow read/write of Satis config file (satis.json)
 * Allows adding/deleting of packages to the file
 *
 * User: matt
 * Date: 11/05/18
 * Time: 17:03
 *
 * @see       https://getcomposer.org/doc/articles/handling-private-packages-with-satis.md
 * @todo      Change setConfig and getConfig functions to magic methods and include @param hinting
 * @todo      Add output-dir setting and remove set/get satisHtmlRoot
 * @todo      Add abandoned/replaced package setting
 * @todo      Add archive option, with sub-options
 *       directory
 *       format
 *       prefix-url
 *       skip-dev
 *       absolute-directory
 *       whitelist
 *       blacklist
 *       checksum
 * @todo      Add Repository 'options' SSH2 settings with sub-options
 *       username
 *       pubkey_file
 *       privkey_file
 * @todo      Add Repository 'options' SSL settings with sub-options
 *       local_cert
 * @todo      Add Repository 'options' HTTP settings with sub-options
 *       header
 * @todo      Add option to only include public/private repos or both
 *            This will allow creation of a privite repo for internal use
 *            and a public repo for any GPL style repos
 *
 *
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  ACTweb/satis
 * @package   SatisBitbucketManager
 * @author    Matt Lowe <marl.scot.1@googlemail.com>
 * @copyright 2018 ACTweb
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   0.0.1
 * @link      http://www.actweb.info/package/SatisBitbucketManager
 */


namespace actweb\satis;

/**
 *
 * Short description for file
 *
 * Long description for file (if any)...
 * Class satis
 * User: matt
 * Date: 16/05/18
 * Time: 23:46
 *
 * @category  ACTweb/satis
 * @package   SatisBitbucketManager
 * @author    Matt Lowe <marl.scot.1@googlemail.com>
 * @copyright 2018 ACTweb
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   0.0.1
 * @link      http://www.actweb.info/package/SatisBitbucketManager
 */
class satis
{

    /**
     * Error codes and there strings
     */
    public const ERROR_CONFIG_FILE_NOT_ACCESSIBLE = 1;
    public const ERROR_CONFIG_FILE_NOT_WRITABLE = 2;
    public const ERROR_JSON_UNREADABLE = 3;
    public const ERROR_INVALID_CONFIG_KEY = 4;
    public const ERROR_FILTER_FAIL = 5;
    public const ERROR_INVALID_REPO_TYPE = 6;
    public const ERROR_INVALID_REPO_URL = 7;
    public const ERROR_MESSAGES
        = array(
            1 => 'Config file not accessible',
            2 => 'Can\'t write to config file',
            3 => 'There was an error reading the JSON file',
            4 => 'Config key not recognised',
            5 => 'Value failed filter test',
            6 => 'Invalid repository type',
            7 => 'Invalid repository URL',
        );
    /**
     * @var string Path and filename for Satis config file
     */
    private $satisConfigFile;

    /**
     * @var array Satis config, exported as JSON for saving.
     */
    private $satisConfig;

    /**
     * Standalone settings within the Satis config file
     *
     * @var array
     */
    private $satisTemplate
        = array(
            'name' => FILTER_SANITIZE_STRING,
            'homepage' => FILTER_SANITIZE_URL,
            'require-all', FILTER_VALIDATE_BOOLEAN,
            'prefix-url' => FILTER_SANITIZE_URL,
            'twig-template' => FILTER_SANITIZE_STRING,
            'output-html' => FILTER_VALIDATE_BOOLEAN,
            'require-dependencies' => FILTER_VALIDATE_BOOLEAN,
            'require-dev-dependencies' => FILTER_VALIDATE_BOOLEAN,
            'providers' => FILTER_VALIDATE_BOOLEAN,
            'config' => FILTER_FLAG_NONE,
            'notify-batch' => FILTER_SANITIZE_URL
        );
    /**
     * Array of packages and versions to set as 'require' within Satis config
     *
     * @var array
     */
    private $satisTemplateRequire
        = array(
            'package' => FILTER_SANITIZE_STRING,
            'version' => FILTER_SANITIZE_STRING
        );
    /**
     * Array of repositories to list within the Satis config
     *
     * @var array
     */
    private $satisTemplateRepositories
        = array(
            'type' => FILTER_SANITIZE_STRING,
            'url' => FILTER_SANITIZE_URL
        );
    /**
     * Array of valid repository types
     *
     * @var array
     */
    private $satisRepoType
        = array(
            'vcs'
        );
    /**
     * Array of required fields for a composer package
     *
     * @var array
     */
    private $packageRequirements
        = array(
            'name',
            'description',
        );
    /**
     * Path to the root web folder of your Satis repository
     *
     * @var string
     */
    private $satisHtmlRoot = '/var/www/satis';
    /**
     * Full path to the satis command
     *
     * @var string
     */
    private $satisCommand = __DIR__ . '/../../satis/bin/satis';
    /**
     * @var int
     */
    private $satisReturnCode;
    /**
     * @var array
     */
    private $satisReturnOutput;
    /**
     * @var bool Include private repo's from the bitbucket account
     */
    private $private=false;
    /**
     * @var bool Include public repo's from the bitbucket account
     */
    private $public=false;

    /**
     * Basic Constructor
     */
    public function __construct()
    {
        // do nothing just now
    }

    /**
     * Read an existing Satis config file
     *
     * @return \actweb\satis\satis
     * @throws \ErrorException
     */
    public function readSatisConfig(): satis
    {
        if (!is_readable($this->satisConfigFile)) {
            throw new \ErrorException(
                SATIS::ERROR_MESSAGES[SATIS::ERROR_CONFIG_FILE_NOT_ACCESSIBLE],
                SATIS::ERROR_CONFIG_FILE_NOT_ACCESSIBLE
            );
        }
        $contents = file_get_contents($this->satisConfigFile);
        $config = \json_decode($contents, true);
        if (\json_last_error() !== JSON_ERROR_NONE) {
            throw new \ErrorException(
                SATIS::ERROR_MESSAGES[SATIS::ERROR_JSON_UNREADABLE],
                SATIS::ERROR_JSON_UNREADABLE
            );
        }
        $this->satisConfig = $config;
        return $this;
    }

    /**
     * Writes the current Satis config to the file system
     *
     * @return \actweb\satis\satis
     * @throws \ErrorException
     */
    public function writeSatisConfig(): satis
    {

        if (!touch($this->satisConfigFile)
            || !is_writable(
                $this->satisConfigFile
            )) {
            throw new \ErrorException(
                SATIS::ERROR_MESSAGES[SATIS::ERROR_CONFIG_FILE_NOT_WRITABLE],
                SATIS::ERROR_CONFIG_FILE_NOT_WRITABLE
            );
        }
        $data = \json_encode(
            $this->satisConfig, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
        );
        $result = file_put_contents($this->satisConfigFile, $data);
        if ($result === false) {
            throw new \ErrorException(
                SATIS::ERROR_MESSAGES[SATIS::ERROR_CONFIG_FILE_NOT_WRITABLE],
                SATIS::ERROR_CONFIG_FILE_NOT_WRITABLE
            );
        }
        return $this;
    }

    /**
     * This allows you to set any basic config options for satis config
     *
     * @param string $name  Config option to set
     * @param string $value Value to set option to
     *
     * @return \actweb\satis\satis
     * @throws \ErrorException
     */
    public function setConfig($name, $value): satis
    {
        if (array_key_exists($name, $this->satisTemplate)) {
            if ($value !== filter_var($value, $this->satisTemplate[$name])) {
                throw new \ErrorException(
                    SATIS::ERROR_MESSAGES[SATIS::ERROR_FILTER_FAIL],
                    SATIS::ERROR_FILTER_FAIL
                );
            }
            $this->satisConfig[$name] = $value;
            return $this;
        }
        throw new \ErrorException(
            SATIS::ERROR_MESSAGES[SATIS::ERROR_INVALID_CONFIG_KEY],
            SATIS::ERROR_INVALID_CONFIG_KEY
        );
    }

    /**
     * Get the current value for requested config option
     * If option hasn't been set, then return null
     *
     * @param string $name Config key to return
     *
     * @return mixed|null
     * @throws \ErrorException
     */
    public function getConfig($name)
    {
        if (array_key_exists($name, $this->satisConfig)) {
            return $this->satisConfig[$name];
        }
        if (array_key_exists($name, $this->satisTemplate)) {
            return null;
        }
        throw new \ErrorException(
            SATIS::ERROR_MESSAGES[SATIS::ERROR_INVALID_CONFIG_KEY],
            SATIS::ERROR_INVALID_CONFIG_KEY
        );
    }

    /**
     * Adds a 'require'd package to the repo
     *
     * @param string $package Package name to require
     * @param string $version Package version to require
     *
     * @return \actweb\satis\satis
     * @throws \ErrorException
     */
    public function addRequire($package, $version): satis
    {
        if ($package !== filter_var(
                $package, $this->satisTemplateRequire['package']
            )) {
            throw new \ErrorException(
                SATIS::ERROR_MESSAGES[SATIS::ERROR_FILTER_FAIL],
                SATIS::ERROR_FILTER_FAIL
            );
        }
        if ($version !== filter_var(
                $version, $this->satisTemplateRequire['version']
            )) {
            throw new \ErrorException(
                SATIS::ERROR_MESSAGES[SATIS::ERROR_FILTER_FAIL],
                SATIS::ERROR_FILTER_FAIL
            );
        }
        $this->satisConfig['require'][$package]
            = $version;
        return $this;
    }

    /**
     * Returns the version number for the requested package
     * IF it is within the 'require' section of the config file
     * Can be used to check if a package has already been added to require
     *
     * @param string $package Required Package to get version for
     *
     * @return string|null
     */
    public function getRequire($package): ?string
    {
        if (array_key_exists($package, $this->satisConfig['require'])) {
            return $this->satisConfig['require'][$package];
        }
        return null;
    }

    /**
     * @param array|string|null $require Array of requirements to add to config (overwrites whats there already!)
     *
     * @return $this
     * @throws \ErrorException
     */
    public function setAllRequire($require = null): self
    {
        if (null === $require) {
            unset($this->satisConfig['require']);
            return $this;
        }
        if (!\is_array($require)) {
            $this->satisConfig['require'] = $require;
            return $this;
        }
        foreach ($require as $package => $version) {
            $this->addRequire($package, $version);
        }
        return $this;
    }

    /**
     * Returns an array of the currently set 'require'd packages or null if none set
     *
     * @return mixed|null
     */
    public function getAllRequire()
    {
        if (array_key_exists('require', $this->satisConfig['require'])) {
            return $this->satisConfig['require'];
        }
        return null;
    }

    /**
     * Bulk set a list of repositories, OVERWRITES existing list!
     *
     * @param array|string|null $repository Array of 'type' => 'url' repos
     *
     * @return $this
     * @throws \ErrorException
     */
    public function setAllRepo($repository = null): self
    {
        if (null === $repository) {
            unset($this->satisConfig['repositories']);
            return $this;
        }
        if (!\is_array($repository)) {
            $this->satisConfig['repositories'] = $repository;
            return $this;
        }
        foreach ($repository as $type => $url) {
            $this->addRepo($type, $url);
        }
        return $this;
    }

    /**
     * Returns an array of all repositories that are configured.
     *
     * @return mixed|null
     */
    public function getAllRepo()
    {
        if (array_key_exists('repositories', $this->satisConfig)) {
            return $this->satisConfig['repositories'];
        }
        return null;
    }

    /**
     * Adds the passed repo type and URL to list of repos
     *
     * @param string $type Type of VCS repo
     * @param string $url  VCS URL
     *
     * @return $this
     * @throws \ErrorException
     */
    public function addRepo($type, $url): self
    {
        if (!$this->existRepo($type, $url)) {
            $this->satisConfig['repositories'][] = array(
                'type' => $type,
                'url' => $url
            );
        }
        return $this;
    }

    /**
     * Check if the passed repository type and url already exist within the repo list
     *
     * @param string $type Type of VCS
     * @param string $url  VCS URL
     *
     * @return bool
     * @throws \ErrorException
     */
    public function existRepo($type, $url): bool
    {
        if (!\array_key_exists('repositories', $this->satisConfig)) {
            return false;
        }
        if (!\in_array($type, $this->satisRepoType, false)) {
            throw new \ErrorException(
                SATIS::ERROR_MESSAGES[SATIS::ERROR_INVALID_REPO_TYPE],
                SATIS::ERROR_INVALID_REPO_TYPE
            );
        }
        if ($url !== filter_var(
                $url, $this->satisTemplateRepositories['url']
            )) {
            throw new \ErrorException(
                SATIS::ERROR_MESSAGES[SATIS::ERROR_INVALID_REPO_URL],
                SATIS::ERROR_INVALID_REPO_URL
            );
        }
        foreach ($this->satisConfig['repositories'] as $item => $value) {
            if (($value['type'] === $type) && ($value['url'] === $url)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public function getSatisConfigFile(): string
    {
        return $this->satisConfigFile;
    }

    /**
     * @param string $satisConfigFile
     */
    public function setSatisConfigFile(string $satisConfigFile): void
    {
        $this->satisConfigFile = $satisConfigFile;
    }

    /**
     * @return string
     */
    public function getSatisHtmlRoot(): string
    {
        return $this->satisHtmlRoot;
    }

    /**
     * @param string $satisHtmlRoot
     */
    public function setSatisHtmlRoot(string $satisHtmlRoot): void
    {
        $this->satisHtmlRoot = $satisHtmlRoot;
    }

    /**
     * @return string
     */
    public function getSatisCommand(): string
    {
        return $this->satisCommand;
    }

    /**
     * @param string $satisCommand
     */
    public function setSatisCommand(string $satisCommand): void
    {
        $this->satisCommand = $satisCommand;
    }

    /**
     * @return int
     */
    public function getSatisReturnCode(): int
    {
        return $this->satisReturnCode;
    }


    /**
     * @return array
     */
    public function getSatisReturnOutput(): array
    {
        return $this->satisReturnOutput;
    }

    /**
     * @return bool
     */
    public function isPrivate(): bool
    {
        return $this->private;
    }

    /**
     * @param bool $private
     */
    public function setPrivate(bool $private): void
    {
        $this->private = $private;
    }

    /**
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->public;
    }

    /**
     * @param bool $public
     */
    public function setPublic(bool $public): void
    {
        $this->public = $public;
    }



    /**
     * Checks the passed JSON data (composer.json contents) and returns true
     * if the file contains all the required options to use this repo as a
     * composer package within Satis.
     * If passed a string, then we use json_decode to convert it to an array
     *
     * @param string|array $json
     *
     * @return bool True if JSON contains the required fields
     */
    public function validateComposerPackageJSON($json): bool
    {
        if (!\is_array($json)) {
            $json = json_decode($json);
            if (json_last_error() !== JSON_ERROR_NONE) {
                return false;
            }
        }
        foreach ($this->packageRequirements as $item) {
            if (!array_key_exists($item, $json)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Execute the Satis build command.
     * If a $package is passed then only rebuild this package
     * $package can be a string of space separated packages
     * $package can be a string of CSV separated packages
     * $package can be an array of separate packages
     *
     * @param string|array|null $package List of packages to build
     *
     * @return int Satis exit code
     */
    public function runSatis($package = null): int
    {
        if (\is_array($package)) {
            $package = implode(' ', $package);
        }
        if (strpos(',', $package)) {
            $package = str_replace(',', ' ', $package);
        }
        $command = $this->satisCommand;
        $command .= ' build ' . $this->satisConfigFile;
        $command .= ' ' . $this->satisHtmlRoot;
        if (null !== $package) {
            $command .= ' ' . $package;
        }
        exec($command, $this->satisReturnOutput, $this->satisReturnCode);
        return $this->satisReturnCode;
    }

}
